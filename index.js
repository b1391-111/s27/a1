const http = require("http");

const PORT = 4000;
const HOST = "localhost";

const server = http.createServer((req, res) => {

    if(req.url === "/" && req.method === "GET"){
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write('<h1>Welcome to the Booking System.</h1>');
        res.end();
    } else if(req.url === "/profile" && req.method === "GET") {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('Welcome to your Profile!');
    } else if(req.url === "/courses" && req.method === "GET") {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end(`Here's our courses available`);
    } else if(req.url === "/addCourse" && req.method === "POST") {
        res.writeHead(201, {'Content-Type': 'text/plain'});
        res.end(`Add a course to our resources`);
    } else if(req.url === "/updateCourse" && req.method === "PUT") {
        res.writeHead(201, {'Content-Type': 'text/plain'});
        res.end(`Update a course to our resources`);
    } else if(req.url === "/archiveCourse" && req.method === "DELETE") {
        res.writeHead(201, {'Content-Type': 'text/plain'});
        res.end(`Archive courses to our resources`);
    } else {
        res.writeHead(404, {"Content-Type" : "text/plain"});
        res.write("I'm sorry the page you are looking for cannot be found");
        res.end();
    }
});
server.listen(PORT);


console.log(`Server is now accessible at ${HOST}:${PORT}`);